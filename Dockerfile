FROM node:8.15.0-alpine as build
ENV PROJECT_HOME /opt/home-budget-fe
WORKDIR $PROJECT_HOME
COPY . $PROJECT_HOME
RUN npm install
RUN npm run build

FROM nginx:alpine
COPY nginx.conf /etc/nginx/nginx.conf
RUN rm -rf /var/www/html
COPY --from=build /opt/home-budget-fe/dist/ /var/www/home-budget-fe/
EXPOSE 80