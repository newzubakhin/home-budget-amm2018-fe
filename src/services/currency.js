import Axios from 'axios'

export default {
  getCurrencies() {
    return Axios.get('/currencies')
      .then(response => response.data)
  },
  createCurrency(body) {
    return Axios.post('/currencies', body)
      .then(response => response.data)
  }
}
