import Axios from 'axios'

export default {
  getAccounts() {
    return Axios.get('/accounts/my/')
      .then(response => response.data)
  },
  createAccount(payload) {
    return Axios.post('/accounts', payload)
      .then(response => response.data)
  },
  deleteAccount(accountId) {
    return Axios.delete(`/accounts/${accountId}`)
  },
  getAccountTransactions(accountId, params = {}) {
    return Axios.get(`/accounts/${accountId}/transactions`, { params })
      .then(response => response.data)
  },
  deleteTransaction(accountId, transactionId) {
    return Axios.delete(`/accounts/${accountId}/transactions/${transactionId}`)
  }
}
